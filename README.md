Open Source at Atlassian
========================

This is the source code of [atlassian.bitbucket.org](http://atlassian.bitbucket.org/).

All listed projects are loaded from [atlassian](https://bitbucket.org/atlassian) and [atlassianlabs](https://bitbucket.org/atlassianlabs) accounts using the Bitbucket API.
